module org.luke.calculator {
    requires javafx.controls;
    requires javafx.fxml;


    opens org.luke.calculator to javafx.fxml;
    exports org.luke.calculator;
}